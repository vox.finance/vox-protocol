/*

website: vox.finance

 _    ______ _  __   ___________   _____    _   ______________
| |  / / __ \ |/ /  / ____/  _/ | / /   |  / | / / ____/ ____/
| | / / / / /   /  / /_   / //  |/ / /| | /  |/ / /   / __/   
| |/ / /_/ /   |_ / __/ _/ // /|  / ___ |/ /|  / /___/ /___   
|___/\____/_/|_(_)_/   /___/_/ |_/_/  |_/_/ |_/\____/_____/   
                                                              
*/

pragma solidity 0.6.12;

import "./token/ERC20.sol";
import "./access/Ownable.sol";

// Vox Populi token for Governance.
contract VoxPopuliToken is ERC20("Vox.Populi", "POPULI", 18, 0, 0), Ownable {
    /// @notice Creates `_amount` of tokens to `_to`. Must only be called by the minter (VoxMaster).
    function mint(address _to, uint256 _amount) public onlyOwner {
        _mint(_to, _amount);
    }
    
    /// @notice Burns `_amount` of tokens from `_from`. Must only be called by the minter (VoxMaster).
    function burn(address _from, uint256 _amount) public onlyOwner {
        _burn(_from, _amount);
    }
}