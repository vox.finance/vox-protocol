// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "../../libs/ERC20.sol";
import "../../libs/SafeMath.sol";

import "../../interfaces/Vault.sol";
import "../../interfaces/Curve.sol";
import "../../interfaces/UniswapV2.sol";

import "../../interfaces/curve/CurveTBTC.sol";
import "../../interfaces/KeepRewardsClaimable.sol";

import "../VoxStrategyCurveBase.sol";

contract VoxStrategyCurveTBTC is VoxStrategyCurveBase {
    // Curve stuff
    address public tbtc_pool = 0xaa82ca713D94bBA7A89CEAB55314F9EfFEdDc78c;
    address public tbtc_gauge = 0x6828bcF74279eE32f2723eC536c22c51Eed383C6;
    address public tbtc_crv = 0x64eda51d3Ad40D56b9dFc5554E06F94e1Dd786Fd;

    // EVENTS
    event Harvested(address indexed token, uint256 amount);

    constructor(
        address _governance,
        address _strategist,
        address _treasury,
        address _devfund,
        address _timelock,
        address _vault
    )
        public
        VoxStrategyCurveBase(
            tbtc_pool,
            tbtc_gauge,
            tbtc_crv,
            _governance,
            _strategist,
            _treasury,
            _devfund,
            _timelock,
            _vault
        )
    {}

    // **** Views ****

    function getMostPremium() public override view returns (address, uint256) {
        return (wbtc, 2);
    }

    function getName() external override pure returns (string memory) {
        return "VoxStrategyCurveTBTC";
    }

    // **** State Mutations ****

    function harvest() public override restricted {
        // bitcoin we want to convert to
        (address to, uint256 toIndex) = getMostPremium();

        // Collects crv tokens
        // this also sends KEEP to keep_rewards contract
        ICurveMintr(mintr).mint(gauge);
        uint256 _crv = IERC20Lib(crv).balanceOf(address(this));
        if (_crv > 0) {
            // x% is sent back to the rewards holder
            // to be used to lock up in as veCRV in a future date
            uint256 _keepCRV = _crv.mul(keepCRV).div(keepCRVMax);
            if (_keepCRV > 0) {
                IERC20Lib(crv).safeTransfer(treasury, _keepCRV);
            }
            _crv = _crv.sub(_keepCRV);
            _swapUniswap(crv, to, _crv);
        }

        // Collects keep tokens
        IKeepRewardsClaimable(keep_rewards).claim_rewards();
        uint256 _keep = IERC20Lib(keep).balanceOf(address(this));
        if (_keep > 0) {
            _swapUniswap(keep, to, _keep);
        }

        // Adds liquidity to curve.fi's pool
        // to get back underlying (tbtccrv)
        uint256 _to = IERC20Lib(to).balanceOf(address(this));
        if (_to > 0) {
            IERC20Lib(to).safeApprove(curve, 0);
            IERC20Lib(to).safeApprove(curve, _to);
            uint256[4] memory liquidity;
            liquidity[toIndex] = _to;
            ICurveTBTC(curve).add_liquidity(liquidity, 0);
        }

        // Distribute the collected funds and deposit underlying
        // Emit harvested event
        _distributeAndDeposit();
        emit Harvested(to, _to);
    }
}
