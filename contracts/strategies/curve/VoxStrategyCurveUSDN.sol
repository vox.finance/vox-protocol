// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "../../libs/ERC20.sol";
import "../../libs/SafeMath.sol";

import "../../interfaces/Vault.sol";
import "../../interfaces/Curve.sol";
import "../../interfaces/UniswapV2.sol";
import "../../interfaces/curve/CurveUSDN.sol";

import "../VoxStrategyCurveBase.sol";

contract VoxStrategyCurveUSDN is VoxStrategyCurveBase {
    // Curve stuff
    address public usdn_pool = 0x094d12e5b541784701FD8d65F11fc0598FBC6332;
    address public usdn_gauge = 0xF98450B5602fa59CC66e1379DFfB6FDDc724CfC4;
    address public usdn_crv = 0x4f3E8F405CF5aFC05D68142F3783bDfE13811522;

    // EVENTS
    event Harvested(address indexed token, uint256 amount);

    constructor(
        address _governance,
        address _strategist,
        address _treasury,
        address _devfund,
        address _timelock,
        address _vault
    )
        public
        VoxStrategyCurveBase(
            usdn_pool,
            usdn_gauge,
            usdn_crv,
            _governance,
            _strategist,
            _treasury,
            _devfund,
            _timelock,
            _vault
        )
    {}

    // **** Views ****

    function getMostPremium() public override view returns (address, uint256) {
        return (dai, 1);
    }

    function getName() external override pure returns (string memory) {
        return "VoxStrategyCurveUSDN";
    }

    // **** State Mutations ****

    function harvest() public override restricted {
        // stablecoin we want to convert to
        (address to, uint256 toIndex) = getMostPremium();

        // Collects crv tokens
        // this also sends KEEP to keep_rewards contract
        ICurveMintr(mintr).mint(gauge);
        uint256 _crv = IERC20Lib(crv).balanceOf(address(this));
        if (_crv > 0) {
            // x% is sent back to the rewards holder
            // to be used to lock up in as veCRV in a future date
            uint256 _keepCRV = _crv.mul(keepCRV).div(keepCRVMax);
            if (_keepCRV > 0) {
                IERC20Lib(crv).safeTransfer(treasury, _keepCRV);
            }
            _crv = _crv.sub(_keepCRV);
            _swapUniswap(crv, to, _crv);
        }

        // Adds liquidity to curve.fi's pool
        // to get back underlying (usdncrv)
        uint256 _to = IERC20Lib(to).balanceOf(address(this));
        if (_to > 0) {
            IERC20Lib(to).safeApprove(curve, 0);
            IERC20Lib(to).safeApprove(curve, _to);
            uint256[4] memory liquidity;
            liquidity[toIndex] = _to;
            ICurveUSDN(curve).add_liquidity(liquidity, 0);
        }

        // Distribute the collected funds and deposit underlying
        // Emit harvested event
        _distributeAndDeposit();
        emit Harvested(to, _to);
    }
}
