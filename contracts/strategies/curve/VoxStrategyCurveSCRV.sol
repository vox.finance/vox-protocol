// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "../../libs/ERC20.sol";
import "../../libs/SafeMath.sol";

import "../../interfaces/Vault.sol";
import "../../interfaces/UniswapV2.sol";
import "../../interfaces/curve/CurveSCRV.sol";

import "../VoxStrategyCurveBase.sol";

contract VoxStrategyCurveSCRV is VoxStrategyCurveBase {
    // sCRV (susdv2) pool, gauge and underlying
    address public susdv2_pool = 0xA5407eAE9Ba41422680e2e00537571bcC53efBfD;
    address public susdv2_gauge = 0xA90996896660DEcC6E997655E065b23788857849;
    address public scrv = 0xC25a3A3b969415c80451098fa907EC722572917F;

    // Tokens we're collecting as well
    address public snx = 0xC011a73ee8576Fb46F5E1c5751cA3B9Fe0af2a6F;

    constructor(
        address _governance,
        address _strategist,
        address _treasury,
        address _devfund,
        address _timelock,
        address _vault
    )
        public
        VoxStrategyCurveBase(
            susdv2_pool,
            susdv2_gauge,
            scrv,
            _governance,
            _strategist,
            _treasury,
            _devfund,
            _timelock,
            _vault
        )
    {}

    // **** Views ****

    function getMostPremium() public override view returns (address, uint256) {
        // Return the most premium stablecoin to convert into
        uint256[] memory balances = new uint256[](4);
        balances[0] = ICurveSCRV(curve).balances(0); // DAI
        balances[1] = ICurveSCRV(curve).balances(1).mul(10**12); // USDC
        balances[2] = ICurveSCRV(curve).balances(2).mul(10**12); // USDT
        balances[3] = ICurveSCRV(curve).balances(3); // sUSD

        // DAI
        if (
            balances[0] < balances[1] &&
            balances[0] < balances[2] &&
            balances[0] < balances[3]
        ) {
            return (dai, 0);
        }

        // USDC
        if (
            balances[1] < balances[0] &&
            balances[1] < balances[2] &&
            balances[1] < balances[3]
        ) {
            return (usdc, 1);
        }

        // USDT
        if (
            balances[2] < balances[0] &&
            balances[2] < balances[1] &&
            balances[2] < balances[3]
        ) {
            return (usdt, 2);
        }

        // SUSD
        if (
            balances[3] < balances[0] &&
            balances[3] < balances[1] &&
            balances[3] < balances[2]
        ) {
            return (susd, 3);
        }

        // If they're somehow equal, we just want DAI
        return (dai, 0);
    }

    function getName() external override pure returns (string memory) {
        return "VoxStrategyCurveSCRV";
    }

    // **** State Mutations ****

    function harvest() public override restricted {
        // stablecoin we want to convert scrv to
        (address to, uint256 toIndex) = getMostPremium();

        // Collects crv tokens
        ICurveMintr(mintr).mint(gauge);
        uint256 _crv = IERC20Lib(crv).balanceOf(address(this));
        if (_crv > 0) {
            // x% is sent back to the rewards holder
            // to be used to lock up in as veCRV in a future date
            uint256 _keepCRV = _crv.mul(keepCRV).div(keepCRVMax);
            if (_keepCRV > 0) {
                IERC20Lib(crv).safeTransfer(treasury, _keepCRV);
            }
            _crv = _crv.sub(_keepCRV);
            _swapUniswap(crv, to, _crv);
        }

        // Collects SNX tokens
        ICurveGauge(gauge).claim_rewards(address(this));
        uint256 _snx = IERC20Lib(snx).balanceOf(address(this));
        if (_snx > 0) {
            _swapUniswap(snx, to, _snx);
        }

        // Adds liquidity to curve.fi's susd pool
        // to get back underlying (sCRV)
        uint256 _to = IERC20Lib(to).balanceOf(address(this));
        if (_to > 0) {
            IERC20Lib(to).safeApprove(curve, 0);
            IERC20Lib(to).safeApprove(curve, _to);
            uint256[4] memory liquidity;
            liquidity[toIndex] = _to;
            ICurveSCRV(curve).add_liquidity(liquidity, 0);
        }

        // We want to get back sCRV
        _distributeAndDeposit();
    }
}
