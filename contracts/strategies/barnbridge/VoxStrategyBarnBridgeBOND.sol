// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "../../libs/ERC20.sol";
import "../../libs/SafeMath.sol";

import "../../interfaces/Vault.sol";
import "../../interfaces/Curve.sol";
import "../../interfaces/UniswapV2.sol";
import "../../interfaces/BarnBridge.sol";

import "../VoxStrategyBase.sol";

contract VoxStrategyBarnBridgeBOND is VoxStrategyBase {
    address
        public constant staking = 0xb0Fa2BeEe3Cf36a7Ac7E99B885b48538Ab364853;
    address
        public constant yieldfarm = 0x3FdFb07472ea4771E1aD66FD3b87b265Cd4ec112;
    address public constant bond = 0x0391D2021f89DC339F60Fff84546EA23E337750f;

    // **** Constructor **** //
    constructor(
        address _governance,
        address _strategist,
        address _treasury,
        address _devfund,
        address _timelock,
        address _vault
    )
        public
        VoxStrategyBase(
            bond,
            _governance,
            _strategist,
            _treasury,
            _devfund,
            _timelock,
            _vault
        )
    {}

    // **** View methods ****

    function balanceOfPool() public override view returns (uint256) {
        return IBBStaking(staking).balanceOf(address(this), underlying);
    }

    function getName() external override pure returns (string memory) {
        return "VoxStrategyBarnBridgeBOND";
    }

    // **** State Mutations ****

    function harvest() public override restricted {
        // Mass Harvest
        IBBYieldFarm(yieldfarm).massHarvest();

        _distributeAndDeposit();
    }

    function deposit() public override {
        uint256 _underyling = IERC20Lib(underlying).balanceOf(address(this));
        if (_underyling > 0) {
            IERC20Lib(underlying).safeApprove(staking, 0);
            IERC20Lib(underlying).safeApprove(staking, _underyling);
            IBBStaking(staking).deposit(underlying, _underyling);
        }
    }

    function _withdrawSome(uint256 _amount)
        internal
        virtual
        override
        returns (uint256)
    {
        IBBStaking(staking).withdraw(underlying, _amount);
        return _amount;
    }
}
