// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

interface IBBStaking {
    function balanceOf(address user, address token)
        external
        view
        returns (uint256);

    function deposit(address tokenAddress, uint256 amount) external;

    function withdraw(address tokenAddress, uint256 amount) external;
}

interface IBBYieldFarm {
    function massHarvest() external returns (uint256);
}