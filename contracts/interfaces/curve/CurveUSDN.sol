pragma solidity 0.6.12;

interface ICurveUSDN {
    function get_virtual_price() external view returns (uint256);

    function add_liquidity(uint256[4] calldata amounts, uint256 min_mint_amount)
        external;

    function balances(int128) external view returns (uint256);
}