// SPDX-License-Identifier: MIT
pragma solidity 0.6.12;

import "../libs/ERC20.sol";

interface IVault is IERC20Lib {
    function underlying() external view returns (address);

    function decimals() external view returns (uint8);

    function earn() external;

    function deposit(uint256) external;

    function depositAll() external;

    function withdraw(uint256) external;

    function withdrawAll() external;

    function distribute() external;

    function salvage(address, uint256) external;

    function getRatio() external view returns (uint256);
}
