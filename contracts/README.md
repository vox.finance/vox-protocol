**Vox Finance Repository**

Website: https://vox.finance

Whitepaper: https://vox.finance/about.html

Twitter: https://twitter.com/VoxFinance

Discord: https://discord.gg/c32KurP

Telegram: https://t.me/VoxFinance

**Vox.Finance $VOX**

_Let your voice shape the future of decentralized finance. #defi #crypto #yieldfarming_

Vox.Finance is an innovative DeFi project created by independent developers that seeks to revolutionize the market by providing a community-led approach.

Below you may find all addresses of deployed contracts. This list will be updated as we launch new products.

------

Vox Token: https://etherscan.io/address/0x12D102F06da35cC0111EB58017fd2Cd28537d0e1

Vox Populi Token: https://etherscan.io/address/0xd6BD082CAC5e76183aa95aFdB3c5488447CecFB7

Vox Master: https://etherscan.io/address/0x5b82b3da49a6a7b5eea8f1b5d3c35766af614cf0
