require('dotenv').config();
const BN = require('bignumber.js');
BN.config({ DECIMAL_PLACES: 0 })
BN.config({ ROUNDING_MODE: BN.ROUND_DOWN })
const { expectRevert, time } = require('@openzeppelin/test-helpers');
const VoxVault = artifacts.require('VoxVault');
const IERC20Lib = artifacts.require('IERC20Lib');
const IERC20 = artifacts.require('IERC20');
const ICurve3Pool = artifacts.require('ICurve3Pool');
const TimeLock = artifacts.require('TimeLock');
const VoxStrategyCurve3CRV = artifacts.require('VoxStrategyCurve3CRV');
const VoxStrategyCurveSCRV = artifacts.require('VoxStrategyCurveSCRV');
const VoxStrategyCurveTBTC = artifacts.require('VoxStrategyCurveTBTC');
const VoxToken = artifacts.require('VoxToken');

const UniswapRouterV2 = artifacts.require('UniswapRouterV2');
const IUniswapV2Factory = artifacts.require('IUniswapV2Factory');
const IWETH = artifacts.require('IWETH');
const e18 = new BN('1000000000000000000');

let routerAddress = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
let factoryAddress = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f";
let wethAddress = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
let voxAddress = "0x12D102F06da35cC0111EB58017fd2Cd28537d0e1";

let threeCRVUnderlyingAddress = "0x6c3F90f043a72FA612cbac8115EE7e52BDe6E490";
let sCRVUnderlyingAddress = "0xC25a3A3b969415c80451098fa907EC722572917F";
let tBTCUnderlyingAddress = "0x64eda51d3Ad40D56b9dFc5554E06F94e1Dd786Fd";

let threePoolCurveAddress = "0xbEbc44782C7dB0a1A60Cb6fe97d0b483032FF1C7";
let daiAddress = "0x6B175474E89094C44Da98b954EedeAC495271d0F";

let timelockMinDelay = 12 * 3600;   //12h;

const HDWalletProvider = require("@truffle/hdwallet-provider");
const assert = require('assert');

async function readNetworkInfo(t) {
    t.weth = await IWETH.at(wethAddress);
    t.factory = await IUniswapV2Factory.at(factoryAddress);
    t.router = await UniswapRouterV2.at(routerAddress);
    t.vox = await VoxToken.at(voxAddress);
    t.threePoolCurve = await ICurve3Pool.at(threePoolCurveAddress);
    t.dai = await IERC20.at(daiAddress);
}

function toWei(n) {
    return new BN(n).multipliedBy(e18).toFixed();
}

contract('VoxVault', ([deployer, admin, governance, strategist, treasury, devFund, staker1, staker2]) => {

    it('Test deposit and withdraw', async () => {
        await readNetworkInfo(this);
        console.log('admin:', admin)
        console.log('deployer:', deployer)

        this.timelock = await TimeLock.new(admin, timelockMinDelay, { from: deployer });
        console.log('delay:', (await this.timelock.delay()).valueOf().toString())
        this.threeCRVVault = await VoxVault.new(threeCRVUnderlyingAddress, voxAddress, governance, treasury, this.timelock.address, { from: deployer });
        this.threeCRVStrategy = await VoxStrategyCurve3CRV.new(governance, strategist, treasury, devFund, this.timelock.address, this.threeCRVVault.address, { from: deployer });
        this.threeCRVUnderlying = await IERC20Lib.at(threeCRVUnderlyingAddress);
        console.log('setting active')
        await this.threeCRVVault.setActive(true, { from: governance });
        console.log('done setting active')
        //let underlying = await this.threeCRVStrategy.methods.underlying().call();
        //console.log('underlying:', underlying)

        //add pending tx to timelock
        let currentTime = await time.latest();
        await this.timelock.queueTransaction(this.threeCRVVault.address, '0', 'setStrategy(address)', `0x000000000000000000000000${this.threeCRVStrategy.address.substring(2)}`, new BN(currentTime).plus(86400), { from: admin });
        console.log('travelling time');
        await time.increase(86400);
        console.log('this.threeCRVStrategy.address:', this.threeCRVStrategy.address)
        await this.timelock.executeTransaction(this.threeCRVVault.address, '0', 'setStrategy(address)', `0x000000000000000000000000${this.threeCRVStrategy.address.substring(2)}`, new BN(currentTime).plus(86400), { from: admin })
        let str = (await this.threeCRVVault.strategy()).valueOf().toString();
        console.log('str:', str);
        currentTime = await time.latest();
        //buy DAI
        await this.router.swapExactETHForTokens(0, [wethAddress, daiAddress], staker1, new BN(currentTime).plus(100).toString(), { from: staker1, value: toWei(30) });
        let balDai = (await this.dai.balanceOf(staker1)).valueOf().toString();
        //add liquidity to curve to get 3crv
        await this.dai.approve(threePoolCurveAddress, balDai, { from: staker1 });
        await this.threePoolCurve.add_liquidity([toWei('300'), '0', '0'], '0', { from: staker1 });
        let bal3CRV = (await this.threeCRVUnderlying.balanceOf(staker1)).valueOf().toString();
        console.log('bal3CRV:', bal3CRV)
        await this.threeCRVUnderlying.transfer(staker2, toWei('200'), { from: staker1 });
        console.log('done swap dai')

        let bal3CRVStaker1 = (await this.threeCRVUnderlying.balanceOf(staker1)).valueOf().toString();
        let bal3CRVStaker2 = (await this.threeCRVUnderlying.balanceOf(staker2)).valueOf().toString();

        await this.threeCRVUnderlying.approve(this.threeCRVVault.address, bal3CRVStaker1, { from: staker1 });

        await this.threeCRVUnderlying.approve(this.threeCRVVault.address, bal3CRVStaker2, { from: staker2 });
        console.log('token:', (await this.threeCRVVault.underlying()).valueOf().toString());
        console.log('underlying:', threeCRVUnderlyingAddress);

        await this.threeCRVVault.deposit(toWei('50'), { from: staker1 });
        await this.threeCRVVault.deposit(toWei('100'), { from: staker2 });

        let balVoxVaultStaker1 = (await this.threeCRVVault.balanceOf(staker1)).valueOf().toString();
        let balVoxVaultStaker2 = (await this.threeCRVVault.balanceOf(staker2)).valueOf().toString();
        assert.strictEqual('2', new BN(balVoxVaultStaker2).dividedBy(new BN(balVoxVaultStaker1)).toFixed(0));

        let staker1Deposit = (await this.threeCRVVault.deposits(staker1)).valueOf().toString();
        let staker2Deposit = (await this.threeCRVVault.deposits(staker2)).valueOf().toString();

        let staker1Issued = (await this.threeCRVVault.issued(staker1)).valueOf().toString();
        let staker2Issued = (await this.threeCRVVault.issued(staker2)).valueOf().toString();
        assert.notStrictEqual('0', staker1Issued);
        assert.notStrictEqual('0', staker2Issued);

        //cannot withdraw more than deposit
        await expectRevert(this.threeCRVVault.withdraw(new BN(staker1Deposit).plus(1).toFixed(0), { from: staker1 }), "withdraw: more than deposited");
        await expectRevert(this.threeCRVVault.withdraw(new BN(staker2Deposit).plus(1).toFixed(0), { from: staker2 }), "withdraw: more than deposited");

        //cannot withdraw if not enough share
        await this.threeCRVVault.transfer(staker2, '10', { from: staker1 });
        await expectRevert(this.threeCRVVault.withdraw(staker1Deposit, { from: staker1 }), "withdraw: not enough shares in balance");

        //staker 2 should be able to withdraw
        await this.threeCRVVault.withdraw(staker2Deposit, { from: staker2 });
        assert.notStrictEqual('0', (await this.threeCRVVault.balanceOf(staker2)).valueOf().toString());
        assert.strictEqual('0', (await this.threeCRVVault.issued(staker2)).valueOf().toString());
        await expectRevert(this.threeCRVVault.withdraw(1, { from: staker2 }), "withdraw: more than deposited");
        assert.strictEqual('10', (await this.threeCRVVault.balanceOf(staker2)).valueOf().toString());

        //staker1 should be able to withdraw after getting enough share
        await this.threeCRVVault.transfer(staker1, 10, { from: staker2 });
        await this.threeCRVVault.withdraw(staker1Deposit, { from: staker1 });
        assert.strictEqual('0', (await this.threeCRVVault.balanceOf(staker1)).valueOf().toString());
        assert.strictEqual('0', (await this.threeCRVVault.issued(staker1)).valueOf().toString());
        await expectRevert(this.threeCRVVault.withdraw(1, { from: staker1 }), "withdraw: more than deposited");

        //withdraw proportionally
        await this.threeCRVVault.deposit(toWei('20'), { from: staker1 });
        staker1Issued = (await this.threeCRVVault.issued(staker1)).valueOf().toString();
        staker1Deposit = (await this.threeCRVVault.deposits(staker1)).valueOf().toString();
        let staker1Balance = (await this.threeCRVVault.balanceOf(staker1)).valueOf().toString();
        let staker1CRVBalance = (await this.threeCRVUnderlying.balanceOf(staker1)).valueOf().toString();

        //withdraw 20%
        await this.threeCRVVault.withdraw(toWei('4'), { from: staker1 });
        let staker1IssuedAfter = (await this.threeCRVVault.issued(staker1)).valueOf().toString();
        let staker1DepositAfter = (await this.threeCRVVault.deposits(staker1)).valueOf().toString();
        let staker1BalanceAfter = (await this.threeCRVVault.balanceOf(staker1)).valueOf().toString();
        let staker1CRVBalanceAfter = (await this.threeCRVUnderlying.balanceOf(staker1)).valueOf().toString();

        assert.strictEqual(staker1IssuedAfter, new BN(staker1Issued).multipliedBy(80).dividedBy(100).toFixed(0));
        assert.strictEqual(staker1DepositAfter, new BN(staker1Deposit).multipliedBy(80).dividedBy(100).toFixed(0));
        assert.strictEqual(staker1BalanceAfter, new BN(staker1Balance).multipliedBy(80).dividedBy(100).toFixed(0));

        let withdrawalFee = (await this.threeCRVVault.withdrawalFee()).valueOf().toString();
        let withdrawalFeeBase = (await this.threeCRVVault.withdrawalFeeBase()).valueOf().toString();
        let feePaid = new BN(toWei('4')).multipliedBy(new BN(withdrawalFee)).dividedBy(new BN(withdrawalFeeBase)).toFixed(0);
        assert.strictEqual(staker1CRVBalanceAfter, new BN(staker1CRVBalance).plus(new BN(toWei('4'))).minus(new BN(feePaid)).toFixed(0));
    });

});